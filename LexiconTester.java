//package Assign1;

import java.util.*;
import java.io.*;

public class LexiconTester {

	public static void main(String[] args) throws IOException {

	//	long startTime = System.currentTimeMillis();
		
		String file = " ";
		
		if (args.length == 1 && args[0].equalsIgnoreCase("in.txt"))
			file = args[0];
		else
			System.out.println("Enter 'in.txt' as the argument");

		ArrayList<String> list = new ArrayList<String>();
		ArrayList<String> frequency = new ArrayList<String>();
		ArrayList<String> words = new ArrayList<String>();
		ArrayList<String> neigh = new ArrayList<String>();
		ArrayList<String> write = new ArrayList<String>();

		try (BufferedReader br = new BufferedReader(new FileReader(file))) {

			String line;
			// reading the file and storing the words in a list
			while ((line = br.readLine()) != null) {
				Scanner read = new Scanner(line);

				while (read.hasNext()) {
					String check = read.next().toLowerCase().replaceAll("[\\p{Punct}\\d]", "");

					if (check.length() >= 1) {
						words.add(check);
					}
				}
			}

			// removing duplicates from the list
			for (String s : words) {
				if (!list.contains(s))
					list.add(s);
			}

			// sort the original list before counting the occurrences of the duplicates
			mergeSort(words, 0, words.size() - 1);

			// counting the duplicate occurrences
			int count = 1;
			for (int i = 0, j = i + 1; i <= words.size() - 1; i++, j++) {
				if (i == words.size() - 1 && j > words.size() - 1) {
					// System.out.println(words.get(i) + " " + count);
					frequency.add(words.get(i) + " " + count);
					break;
				}
				if (words.get(i).equalsIgnoreCase(words.get(j)))
					count++;
				else {
					frequency.add(words.get(i) + " " + count);
					// System.out.println(words.get(i) + " " + count);
					count = 1;
				}
			}

			// sorting the words list & words list with frequencies
			mergeSort(list, 0, list.size() - 1);
			mergeSort(frequency, 0, frequency.size() - 1);

			// find neighbours
			for (int i = 0; i < list.size(); i++) {

				for (int j = list.size() - 1; j >= 0; j--) {
					if (list.get(i).length() == list.get(j).length()) {
						int check = 0;
						for (int k = 0; k < list.get(i).length(); k++) {
							if (list.get(i).charAt(k) != list.get(j).charAt(k))
								check++;
						}

						// add to neighbours if they differ by only 1 character
						if (check == 1)
							neigh.add(list.get(j));
						// reset the value
						// check = 0;
					}
				}

				// sorting the neighbours
				mergeSort(neigh, 0, neigh.size() - 1);

				// System.out.println(frequency.get(i) + " " + neigh);

				// storing the end result to a list
				write.add(frequency.get(i) + " " + neigh);

				neigh = new ArrayList<String>();
			}

			// write the output to a file
			writeToFile(write);

//			long endTime = System.currentTimeMillis();
//			long timeElapsed = endTime - startTime;
//
//			System.out.println("Execution time in seconds: " + timeElapsed / 1000);
			
		} catch (IOException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}

	}

	public static <T extends Comparable<T>> void mergeSort(List<T> list, int first, int last) {
		if (first < last) {
			int middle = (first + last) / 2;
			mergeSort(list, first, middle);
			mergeSort(list, middle + 1, last);
			merge(list, first, middle, last);
		}
	}

	private static <T extends Comparable<T>> void merge(List<T> list, int firstLeft, int lastLeft, int lastRight) {
		int firstRight = lastLeft + 1;

		int numberOfElements = lastRight - firstLeft + 1;
		List<T> tempA = new ArrayList<T>(numberOfElements);

		int leftIndex = firstLeft;
		int rightIndex = firstRight;
		int index = 0;

		while (leftIndex <= lastLeft && rightIndex <= lastRight) {
			if (list.get(leftIndex).compareTo(list.get(rightIndex)) < 0) {
				tempA.add(index, list.get(leftIndex));
				++leftIndex;
				++index;
			} else {
				tempA.add(index, list.get(rightIndex));
				++rightIndex;
				++index;
			}
		}

		while (leftIndex <= lastLeft) {
			tempA.add(index, list.get(leftIndex));
			++leftIndex;
			++index;
		}

		while (rightIndex <= lastRight) {
			tempA.add(index, list.get(rightIndex));
			++rightIndex;
			++index;
		}

		for (index = 0; index < numberOfElements; ++index) {
			list.set(firstLeft + index, tempA.get(index));
		}

	}

	public static void writeToFile(ArrayList<String> list) {
		try (BufferedOutputStream bs = new BufferedOutputStream(new FileOutputStream("out.txt"))) {
			String fileContent = "";
			for (int i = 0; i < list.size(); i++) {
				fileContent = list.get(i) + "\n";
				bs.write(fileContent.getBytes());
			}
		} catch (IOException e) {
			System.out.println("Error:Cannot write to file");
			e.printStackTrace();
		}
	}

}
